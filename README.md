
# Table of Contents

1.  [musicbox](#org928f339)
2.  [Install](#orga5f9b9d)
    1.  [Install directly from the GIT URL](#orgfae8494)
    2.  [From source code](#org8606ad8)
    3.  [Install to run the program in the source code](#org47823f4)
    4.  [Install in Window](#org31a8f54)
3.  [License](#org5ee7fe9)


<a id="org928f339"></a>

# musicbox

Guess intervals to practice your music ear.

This program selects a music interval randomly, then plays it to let the user guess it. The idea is to practice interval recognition.

-   Page: <https://nubecita.online/~cnngimenez/musicbox>
-   Gemini page: gemini://g.nubecita.online/musicbox/index.gmi

This is a Readme file with little information about the program and its usage. Just installation instructions can be found here.
More documentation can be found in the PDF, HTML, or scribble. See <https://nubecita.online/~cnngimenez/musicbox/latest/docs/musicbox.html> 


<a id="orga5f9b9d"></a>

# Install

-   **Windows:** Grab the installer from:
    -   <https://nubecita.online/~cnngimenez/musicbox>
-   **Linux:** Install with Raco using the Git URL, or from source.


<a id="orgfae8494"></a>

## Install directly from the GIT URL

For racket version >= 8.0 only.

    raco pkg install https://gitlab.com/cnngimenez/musicbox.git

The program `musicbox` should be installed in the Racket bin directory as explained before.


<a id="org8606ad8"></a>

## From source code

For Racket version < 8.0:

    git clone https://gitlab.com/cnngimenez/musicbox.git
    cd musicbox
    raco pkg install

The program should be available in your PATH. Just run:

    musicbox

If the program is not found, check if your Racket bin directory is in your PATH environment variable. The Racket bin directory is usually `~/.local/share/racket/8.5/bin` for Racket 8.5. The raco tool installs any binary package (in this case the `musicbox` binary) inside that bin path.


<a id="org47823f4"></a>

## Install to run the program in the source code

To install and run the program inside the source code:

    git clone https://gitlab.com/cnngimenez/musicbox.git
    cd musicbox
    raco pkg install rsound

To run the program, enter the musicbox directory and run:

    racket main.rkt

This install let you change the code.


<a id="org31a8f54"></a>

## Install in Window

The installer can be downloaded at <https://nubecita.online/~cnngimenez/musicbox>
Follow the instructions the program provides.

For advanced users and developers, if you have Racket installed in your system, `raco` should work as explained in the above sections.


<a id="org5ee7fe9"></a>

# License

This work is under the GNU General Public License version 3 (GPLv3).

