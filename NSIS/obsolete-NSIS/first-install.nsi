# Copyright 2022 Christian Gimenez
#
# first-install.nsi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


Name "Instalar Musicbox"
Caption "Instalar Musicbox"
# Icon
OutFile "Instalar.exe"
ManifestSupportedOS all
RequestExecutionLevel admin
Unicode true

# SetCompressor /SOLID lzma

InstallDir "$LOCALAPPDATA\Musicbox"

LicenseData "license.txt"
LicenseForceSelection checkbox "Acepto"

BGGradient 000000 800000 FFFFFF
InstallColors FF8080 000030

BrandingText "Musicbox"

LicenseText "Por favor, revise la licencia antes de instalar Musicbox. Si acepta los términos, haga clic en el casillero de abajo. Luego, clic en Siguiente para continuar." "Siguiente"
DirText "Este programa instalará Musicbox y sus dependencias en la siguiente carpeta. Para instalar los archivos en una carpeta diferente, haga clic en Browse y seleccione otro directorio. Clic en Instalar para comenzar la copia e instalación."
ComponentText "Verifique los componentes que quiera instalar y destilde aquellos que no desee. Luego, haga clic en Siguiente para continuar." "" "Componentes a instalar:"

MiscButtonText "< Volver" "Siguiente >" "Cancelar" "Cerrar"
InstallButtonText "Instalar"
DetailsButtonText "Mostrar detalles"
CompletedText "Instalación Completada"
SpaceTexts "Espacio requerido:" "Espacio disponible:"

Page license
Page components
Page directory
Page instfiles
Page custom thanksPage

UninstPage uninstConfirm
UninstPage instfiles

Section "!Racket 8.5"
  AddSize 13316
  AddSize 89396
  DetailPrint "Instalando Racket 8.5"
  SetOutPath "$INSTDIR\dist"
  File "dist\racketinstaller-8.5-x64.exe"
  ExecWait '"$INSTDIR\dist\racketinstaller-8.5-x64.exe" /verysilent' $0
  DetailPrint "Instalación de Racket retornó $0."
SectionEnd

Section "!Paquetes Racket necesarios"
  AddSize 20268
  AddSize 144468
  DetailPrint "Extrayendo paquetes para Racket"  
  SetOutPath "$INSTDIR\dist\packs"
  File /r "dist\packs\*.*"
  DetailPrint "Instalando los paquetes"
  nsExec::ExecToLog 'install_packs.bat'
SectionEnd

Section "!Aplicación principal"
  DetailPrint "Extrayendo código Racket de la aplicación principal."
  SetOutPath $INSTDIR
  File /a /r "musicbox-main\*.*"
  DetailPrint "Código de Musicbox extraído."
  CreateShortcut "$DESKTOP\Musicbox.lnk" "racket $INSTDIR\main.rkt"
  DetailPrint "Acceso directo creado."
SectionEnd

Function thanksPage
  MessageBox MB_OK "Se han copiado todos los archivos. Puede ejecutar la aplicación haciendo doble clic en el acceso directo que se encuentra en el escritorio.$\n$\n     ¡Gracias por instalar Musicbox!"
FunctionEnd
