# Copyright 2022 Christian Gimenez
#
# first-install-mui2.nsi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

!include "MUI2.nsh"

Name "Musicbox"
Caption "Musicbox"
# Icon
OutFile "Instalar.exe"
ManifestSupportedOS all
;; RequestExecutionLevel admin
RequestExecutionLevel user
Unicode true

# SetCompressor /SOLID lzma

InstallDir "$LOCALAPPDATA\Musicbox"

; --------------------------------------------------
; Variables

Var StartMenuFolder

;--------------------------------------------------
; Interface settings

!define MUI_ABORTWARNING
!define MUI_COMPONENTS_PAGE_SMALLDESC
!define MUI_INSTFILESPAGE_COLORS "FF8800 0000FF"
; !define MUI_ICON "../musicbox.ico"


; --------------------------------------------------
; PAGES

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "..\COPYING.txt"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
!insertmacro MUI_PAGE_INSTFILES
page custom thanksPage
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

; --------------------------------------------------
; Languages

!insertmacro MUI_LANGUAGE "Spanish"

; --------------------------------------------------
; Sections

Section "!Musicbox" SecMusicbox
  DetailPrint "Extrayendo aplicaci�n y archivos necesarios."
  SetOutPath $INSTDIR
  File /a /r "..\dist\*.*"
  DetailPrint "Aplicaci�n Musicbox extra�da."
  CreateShortcut "$DESKTOP\Musicbox.lnk" "$INSTDIR\musicbox.exe"
  CreateShortcut "$DESKTOP\Musicbox_documentation.lnk" "$INSTDIR\docs\musicbox.pdf"
  DetailPrint "Acceso directo creado."
  WriteUninstaller "$INSTDIR\uninstall.exe"

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application    
  ; --  Create shortcuts
  CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
  CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Musicbox.lnk" "$INSTDIR\musicbox.exe"
  CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Musicbox_documentation_HTML.lnk" "$INSTDIR\docs\musicbox.html"
  CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Musicbox_documentation_PDF.lnk" "$INSTDIR\docs\musicbox.pdf"
  CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\uninstall.exe"  
  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

; --------------------------------------------------
; Descriptions
langString DESC_SecMusicbox ${LANG_SPANISH} "Archivos de Musicbox y DLLs necesarios."

;Assign language strings to sections
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
!insertmacro MUI_DESCRIPTION_TEXT ${SecMusicbox} $(DESC_SecMusicbox)
!insertmacro MUI_FUNCTION_DESCRIPTION_END

; --------------------------------------------------
; Uninstaller Section
Section "Uninstall"
  Delete "$INSTDIR\uninstall.exe"
  Delete "$INSTDIR\*.*"
  Delete "$DESKTOP\Musicbox.lnk"
  Delete "$DESKTOP\Musicbox_documentation.lnk"
  RMDir /r "$INSTDIR"

  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder    
  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\Musicbox.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\Musicbox_documentation_HTML.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\Musicbox_documentation_PDF.lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
SectionEnd


; --------------------------------------------------
; Functions
LangString PAGE_TITLE ${LANG_SPANISH} "�Todo copiado!"
LangString PAGE_SUBTITLE ${LANG_SPANISH} "�Muchas gracias!"

Var Dialog
Var Text

Function thanksPage
  !insertmacro MUI_HEADER_TEXT $(PAGE_TITLE) $(PAGE_SUBTITLE)
  
  nsDialogs::Create 1018
  Pop $Dialog
  
  ${If} $Dialog == error
    Abort
  ${EndIf}

  ${NSD_CreateLabel} 0 13u 100% -13u "Se han copiado todos los archivos. Puede ejecutar la aplicaci�n haciendo doble clic en el acceso directo que se encuentra en el escritorio.$\n$\nTambi�n se ha incluido un desinstalador 'uninstall.exe' para que pueda remover los archivos cuando necesite espacio. Se incluy� la documentaci�n y un acceso directo para que pueda accederla.$\n$\nRecuerde, ante cualquier duda o inconveniente, visite https://gitlab.com/cnngimenez/musicbox$\n$\n     �Gracias por instalar Musicbox!$\n$\n"
  Pop $Text
  
  nsDialogs::Show
FunctionEnd
