#lang scribble/doc
@(require scribble/manual)

@title[#:tag "musicbox-users" #:version "0.1"]{Exercises and Graphical User Interface Explanations}
@author[(author+email "cnngimenez" "cnngimenez at-symbol-here disroot.org" #:obfuscate? #t)]

Table of contents for this section:

@local-table-of-contents[]

@; Install instruction would be great, right?
@section{Installing Musicbox}
@margin-note{Sorry for the inconvenience! This section needs more caring!}
This section explains how to install Musicbox in your computer.

@subsection{Installing in Windows}
An installer is available at:
https://nubecita.online/~cnngimenez/musicbox

@margin-note{Programmers and developers: Explanation on how to create an installer is at Section @secref{NSIS}.}

@subsection{Installing in GNU/Linux}
Need more 

@; --------------------------------------------------
@; @section{Exercises and Graphical User Interface Explanations}
@section{Interval Recognition Exercise}
The objective of this exercise is to train your music interval recognition. The program will play a random interval for you to recognise it!

The interface let you configure the exercise: what intervals to use, what are the starting notes, and if the interval should be played ascending, descending, random, or both. Then it plays a random interval from the selection provided and waits for the user to say which interval it played.

The user should hear and recognise the interval played by the computer. If the user clicked on the correct interval button, it plays the next interval. If not, it waits for another chance.

When the correct interval button is pressed, it is called a @bold{match}. Otherwise, when the user could not recognise the played interval and pressed the incorrect button, it is called a @bold{miss}.

@; --------------------------------------------------
@section{Intervals Singing Exercise}
This interface let you hear intervals using different configurations.

@; @defmodule{play-exercise-gui}

This exercise will play an interval for you to hear.
Try to reproduce with your voice saying the note or simply "ah".

It is possible to repeat the same exercise with lip-rolling, then "r", afte that "ka-ke-ki-ko-ku" or any kind of mix of vocals, consonants, or both. Also, you can try @hyperlink["http://lilypond.org/doc/v2.22/Documentation/music-glossary/glissando" "glissando"] and any other techniques.

We recommend an Android (cellphone) software like Semitone [1]. This program shows the note you are singing and how much you are off from the correct frequency. You can download the APK from the Webpage or you can install F-Droid [2], a Free Software and Open-Source repository. Worth to mention, Semitone is also Free (as in freedom) Software.

[1] @url{https://f-droid.org/en/packages/mn.tck.semitone} last visited 5 november 2022.

[2] @url{https://f-droid.org/} last visited 5 november 2022.

@section{Statistics}
@margin-note{This feature is still being developed!}
It would be great if the software could tell you when you missed some intervals. Which intervals were played and which one the user thought they were.

When starting, it is common to mistake one interval with another (for example, P4 with P5 or M3 with P4). But with practice, these intervals become clearer: the user start to recognise them better.

The software can store user statistic information: which interval were played and what the user answered. The aim is to deduce the most common missed intervals.

@section{More learning resources and utilities?}
This is a list of resources to learn an practice music.

@bold{Semitone Android App}

@url{https://f-droid.org/en/packages/mn.tck.semitone}

This app is Free Software and provides a tuner, a metronome, and a piano keyboard. Useful for testing if playing the correct note.


@bold{Tunerly}

@url{https://f-droid.org/es/packages/com.tunerly/}

To tune your guitar, bass, ukulele or cuatro!


@bold{Tuner}

@url{https://f-droid.org/es/packages/org.billthefarmer.tuner/}


@bold{Music exercise (and audio resources)}

@url{https://gitlab.com/cnngimenez/music-exercises}

Some simple scores to read and play. Scales and singing exercises too. The scores are also available as audio files to hear them.
The source code is written in Lilypond, which can be used as an example for writing your own scores.


@bold{Lilypond}

@url{http://lilypond.org/}

To write your own music sheets with high quality.
