#lang scribble/manual

@require[@for-label["../music/notes.rkt"
                    "../music/scales.rkt"
                    "../music/midi.rkt"
                    "../guis/interval-gui.rkt"
                    "../guis/play-exercise-gui.rkt"
                    "../config.rkt"
                    racket/base
                    racket/gui]]

@title[#:tag "musicbox-programmers" #:version "0.1"]{Programmers Documentation: Modules and API}
@author[(author+email "cnngimenez" "cnngimenez at-symbol-here disroot.org" #:obfuscate? #t)]

Documentation for developers and Racket programmers. This texts describes the modules and API provided and used to build Musicbox.

Table of contents for this section:

@local-table-of-contents[]

@; --------------------------------------------------
@section{music/* - Music modules}
@; --------------------------------------------------

@; --------------------------------------------------
@subsection{Notes Module}
@; --------------------------------------------------
@defmodule{music/notes}
Musical note functions and manipulation.

@defthing[notes (listof string?)]{
  A list of strings with notes names. The order is a chromatic scale from C (do) to B (si) without repeating notes.}

@defproc[(note? [str string?])
                boolean?]{
The given string represent a note name?
It can recognize A-G letters with "#" or "b" alterations and scale number.}

@defproc[(notestr->note [str note?])
                       string?]{
Extract the note part without alteration nor scale number.}

@defproc[(notestr->alter [str note?])
                        (or/c #f string?)]{
Return only the alteration of the given note string.
Alteration may be "#" or "b". If the note has no alteration specified, then return #f.}

@defproc[(notestr->scale [str note?])
                        number?]{
Return the scale number of the note.
}

@defproc[(number->notestr [lst-data (list/c number? number?)])
                          string?]{
From a list of two elements (two numbers) return the note string that it represents.
The first number of the list is the note, starting 0 is C and 11 is B (including alteranions, ex.: 1 is C#).
The second number is the scale of the note.
}

@; --------------------------------------------------
@subsection{Scales Module}
@; --------------------------------------------------
@defmodule{music/scales}
Functions to calculate intervals and scales.

@defthing[intervals (listof symbol?)]{
  A list if interval names.

  Each element is a symbol representing the abbreviated interval name. The order is from Perfect Unison (P1) to Perfect Octave (P8).
}

@defthing[scale-type (listof
                        (listof symbol? (listof symbol?)))]{
A list of alist defining the scale type and their intervals.
The 'major, 'minor and other type of scales are defined here.
}

@defproc[(interval? [sym any/c])
                    boolean?]{
Is the given parameter an interval symbol?
}

@defproc[(scale-type? [type any/c])
                      boolean?]{
Is the given parameter a scale-type symbol?
}

@defproc[(direction? [sym any/c])
                     boolean?]{
Direction can be @racket['ascending] and @racket['descending].
}

@defproc[(calc-interval [start-note string?]
                        [interval symbol?]
                        [direction symbol? 'ascending])
                        (listof string?)]{
  Return the interval from the start-note.
}

@defproc[(generate-scale [first-note string?]
                         [type symbol?])
                         (listof string?)]{
  Return a scale of the given type.
  Type can be 'major, 'minor or any type defined in @racket[scale-types].
}

@defproc[(generate-interval-from-list [notes (listof string?)]
                                      [interval symbol?])
                                      (listof (listof string?))]{
  Return a list of intervals calculated from the given notes.

  For each note calculate the interval and return all of them in a list.
}

@defproc[(generate-interval-succession [start-note string?]
                                       [interval symbol?]
                                       [scale-type symbol? 'major]
                                       [amount number? 8])
                                       (listof (listof string?))]{
  Generate a succession of intervals from a particular scale notes.
  
  Generate a scale from the start note, calculate the interval of each note and return them. If amount is given, return the given amount of intervals. If scale-type is given, the starting note of each interval is calculated according to the given scale.
}

@defproc[(append-reverse [intervals (listof (listof string?))]
                         [repeat-last-note boolean? #f])
                         (listof (listof string?))]{
  For each sequence of notes, calculate their reverse and appends them to the sequence.
This is useful when practising intervals. For instance, when you want to pratise up-and-down intervals , like "C1" "E1" "G1" and then down: "E1" "C1". The "G1" (the last one on the original sequence) is not repeated.

If repeat-last-note is true, then the last note of the interval is repeated. For example: if the intervals are "C1" "E1" "G1" and repeat-last-note is #t, then the return value would be "C1" "E1" "G1" "G1" "E1" "C1" instead of "C1" "E1" "G1" "E1" "C1".
}

@; --------------------------------------------------
@subsection{MIDI Module}
@; --------------------------------------------------
@defmodule{music/midi}
MIDI conversion utilities.

@defproc[(notestr->midi [str string?])
                        number?]{
  Return the MIDI number for the given note string. For instance, "C1" is the MIDI number 24.
}

@; --------------------------------------------------
@section[#:tag "gui"]{guis/* - Graphical User Interfaces}
@; --------------------------------------------------

@; --------------------------------------------------
@subsection{Interval GUI Module API}
@; --------------------------------------------------
This is the graphical user interface for the interval exercise.

@defmodule{guis/interval-gui}

@defthing[interval-listbox object?]{
A @racket[list-box%] instance with all the intervals. The user should select the intervals they want to train here.
}

@defthing[guess-choice object?]{
The guess-choice is a @racket[choice%] instance used by the user to select the interval they reconise.
}

@defproc[(populate-guesses [interval-list list?])
                           void?]{
  Clear the guess-choice and add the interval-list list. The user should use this guess-choice to select the interval they recognise.  
}

@defproc[(populate-guess-choice) void?]{
  Fill the @racket[guess-choice] according to the intervals configured by the user. If "All" is selected, add all the intervals to the @racket[guess-choice].
}

@; --------------------------------------------------
@subsection{Play Interval GUI Module API}
@; --------------------------------------------------
This is the graphical user interface for the play interval exercise.

@defmodule{guis/play-exercise-gui}

@defthing[explain-string string?]{
An explanation string used to show to the user on the interface.
}

@defproc[(play-exercise-show) any/c?]{
Show the GUI to the user.
}

@subsubsection{GUI Getter Functions}
These are functions designed to obtain information from the user input in the GUI. They obtain data from the GUI objects and process them to be usable by other Racket functions.

@defproc[(get-start-note)
                note?]{
Return the note selected as a starting one.
}

@defproc[(get-direction)
            direction?]{
Return the user selected direction of the intervals.
}

@defproc[(get-repeat-last-note)
                    boolean?]{
Return true if the user check the repeat last note checkbox.
}

@subsubsection{Scale and exercise generation}

@defproc[(generate-notes)
            (listof note?)]{
Generate the exercise notes according to the configuration provided by the user.
}

@subsubsection{Updating GUI widgets}
These functions updates widgets data and behaviour.

@defproc[(update-selected-scale) any/c?]{
Update the scale showed to the user according to the selected configuration options.
}

@subsubsection{Starting and stoping exercise}

@defproc[(start-exercise) any/c?]{
Configuration is considered done, thus disable the configuration portion of the GUI and enable the play and exercise portion.    
}

@defproc[(stop-exercise) any/c?]{
The opposite of @racket{start-exercise}: disable the play and exercise portion of the GUI and enable the configuration portion.
}

@; @defmodule{config-gui}

@; --------------------------------------------------
@section{User Statistics}
@; --------------------------------------------------
@defmodule{statistics/interval-statistics.rkt}

This module stores data about the user interval recognition. How many times they recognises well the interval and how many times they misses it. Also, which interval misses and with which one they confuses it.

The purpose of this module is to store information needed to deduce which music intervals are more difficult to recognise by the user. For instance, if the user hears a P5 (perfect fifth interval, C-G notes for example) but the user recognises it as P4 (perfect fifth, C-F notes) then, they try again and say it is an M3, then as P4 and then as P5. This module store three misses with P4, M3, P4 for the P5 interval.

This information indicates that user usually misses the P5 interval with P4. Also, it can be used to tell the user which intervals they recognises better.

@defthing[interval-stat struct?]{The structure used to store all interval recognition statistics.}

@defthing[stat-intervals interval-stat?]{
An alist of intervals and their associated @racket[interval-stat] structure. This is the general variable used to update and store the data.}

@defproc[(stat-interval-try [current-interval interval?]
                            [user-interval interval?]) any/c]{
Store a user try. If the user recognises the interval correctly, current-interval should be the same as user-interval, and add one to the matches count on current-interval.

When the user could not recognise the interval properly, stores the user-interval in order to calculate later which interval confuses more to them.}

@defproc[(stat-intervals->string) string?]{Return a string representation of @racket[stat-intervals].}

@defproc[(stat-misses [interval interval?])
                      (listof interval?)]{
Given the interval, return a list of intervals recognised by the user when the input were played.
When then user heard the given interval, and failed to recognise it, the played interval and the user input are stored. The list returned are these intervals missed when the interval given as input were played.
}

@defproc[(interval-stat->string [stat interval-stat?])
                              string?]{
Convert an interval-stat structure into a string representation.
}

@defproc[(stat-total-practised)
    natural?]{
Return the total amount of times the user played the interval and matched or missed them.

This is the total count of all intervals.
}
    
@defproc[(stat-total-matches)
    natural?]{
Return the total amount of times the user could recognise the played intervals.

This is the total count of all intervals.
}

@defproc[(stat-total-misses)
    natural?]{
Return the total amount of times the user could not recognise the played interval.

This the total count of all intervals.
}

@defproc[(matches-to-happiness [interval interval?])
                               natural?]{
Return a percentage probability of matches for certain interval.
The return value is a number between 0 and 100, 0 meaning no matches at all and 100 all matches. A value like 100 means that the user can recognise the interval perfectly.

This meassure is used to create the "happy face" for buttons.
}

@defproc[(stat-interval-reset)
    any/c]{
Reset the @racket{stat-intervals} variable to zeroed values.
}

@defproc[(count-intervals [lst-intervals (listof interval?)])
                          (listof (cons/c interval? natural?))]{

}

@defproc[(stat-intervals->table [header boolean? #f])
                               (listof any/c)]{
Make a table representation from @racket{stat-intervals} variable.
This is suitable for CSV storage. See csv-writing Racket package.
}


@; --------------------------------------------------
@section{Config module}
@; --------------------------------------------------
@defmodule{config.rkt}
Module to load and store configuration data.

@defthing[config-type struct?]{The configuration structure.}

@defthing[current-config config-type?]{The current configuration for the program.}

@defthing[musicbox-config-path path-for-some-system?]{
The file path where the configuration is stored.

The configuration file is a Racket file.}

@defthing[musicbox-interval-statistics-path path-for-some-system?]{
The file path where the statistics for interval recognition exercise is stored.

It is usually a CSV file stored next to the configuration file.}

@defproc[(config->str [config config-type?])
                     string?]{Return a string representation of a config instance.}

@defproc[(empty-config? [config config-type?])
                        boolean?]{
Is the current config a recently created one?
An empty configuration has no information and cannot be used. It may be viewed as an invalid configuration.}

@defproc[(load-config) config-type?]{
Load the configuration from the user local preferences path.

This path varies from each Operative System. In GNU/Linux is in @tt{~/.config/racket/musicbox/config.rkt}. See @racket{musicbox-config-path} to know the current path.}



@; --------------------------------------------------
@section[#:tag "NSIS"]{NSIS and Windows installer builder}
@; --------------------------------------------------

See the NSIS directory.
@margin-note{Still under developing.}
