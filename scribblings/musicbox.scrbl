#lang scribble/manual

@title["Musicbox" #:version "0.1"]
@author[(author+email "cnngimenez" "cnngimenez at-symbol-here disroot.org" #:obfuscate? #t)]

@; @abstract{Interval exercises are a tipical and a very usual way to practise the recognition of songs and notes. It is esencial for any musician in training to exercise it, at least a little, everyday. Therefore, tools, programs, and the required documentation would be useful for easy access to the required resources to practise all day.

@; This program aims to bring help to anyone who want to train hearing and singing. And, this document aims to explain the program, its graphical interfaces, and the exercises for the users. Also, it explains the modules and programming API for Racket developers who want to tinker and to create their own tools.

@; Of course, this work is far from being comprehensive: It is in costant evolution with several ideas and features to implement. Nonetheless, if it is usable and useful to someone in some way, the authors and collaborators would feel happy!}

@bold{Welcome to Musicbox!}

@emph{-- Some utilities and exercises to help you on your music hearing and singing.}

Learning to play an instrument, like a guitar, xilophone, or your own voice!, it is imperative to use the instrument itself: practice, and more practice, at least a little everyday. But, that is not it! You also need to learn several other things that helps in other aspects of the instrument: yes, music is not only about the instrument, it also about you! Your hearing, your behaviour, your charisma, your good days and bad days, your traditions and culture... The instrument itself is not enough to play music, the interpreter, the person behind the instrument is also very important.

In our case, we were learning and training a part of the music involved in our hearing: interval recognition. We were training intervals, not only because it is the most common exercise for musicians, but because they are important to hear your favourite song and to understand how much "higher" or "lower" the singer note is in respect to another note.

However, to train it we needed someone to play an interval on an instrument, tipically a piano, and it must be a random interval, in order to hear it and try to recognise it. This kind of exercise can be implemented on a computer: the computer plays a random interval, the user tries to recognise it, and ask the computer if they recognise it correctly.

Once implemented, this leads to think: maybe another kind of exercise can be implented too! With this in mind, we tried to create this program: the objective is to implement exercises, or at least an utility, to help users to train them on music hearing or singing.
It is not intended to be comprehensive, nor completely customisable by non-specialised users, or anything that great: just usable, and a little fun. 

To better understand this manual and its contents, it is structured as follows: The first sections describe the graphical user interfaces (GUI), i.e. the exercises and how it is presented to the user. After that, the racket modules, their functions, and any documentation useful for programmers: the API.

In any case, if this manual does not cover your doubts, or you have suggestions, you are welcome to contact us. These are the project pages, repositories and contact places:

@tabular[#:sep @hspace[1]
(list (list "Webpage" @url{https://nubecita.online/~cnngimenez/musicbox})
      (list "Gemini page" @url{gemini://g.nubecita.online/musicbox/index.gmi})
      (list "Mastodon" @url["https://mastodon.social/@cnngimenez"])
      (list "E-mail" "cnngimenez at-symbol-here disroot.org")
      (list "Repository" @url{https://gitlab.com/cnngimenez/musicbox})
      (list "Issues report" @url{https://gitlab.com/cnngimenez/musicbox/-/issues/new}))]

If you find bugs (errors), or you want to make suggestions, you can send it to the repository Issues section. Reporting issues may require registering at gitlab.com. This is a free-service and you only require to have an e-mail address. Nevertheless, our e-mails are always open for suggestion, just make sure to add "Musicbox" at the subject of the mail to easy find them!

Have fun!

@table-of-contents[]

@include-section["musicbox-users.scrbl"]
@include-section["musicbox-programmers.scrbl"]

